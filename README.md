What is known so far, on a B&O H9 3rd Gen set:

gatttool -b {MACADDR} --char-read --uuid=95c09f26-95a4-4597-a798-b8e408f5ca66 | sed "s/.*value: //"

(or with filter: gatttool -b {MACADDR} --char-read --uuid=95c09f26-95a4-4597-a798-b8e408f5ca66 | sed "s/.*value: //" | sed "s/^0f 00 //" | sed "s/00 00 84 7a bd 84 1f 11 84 //")

optimal : ff c0 ff c0 ff c0 ff c0      (centered)
mids:       ff f6 ff 8b ff 91 7f 8c     (left)
nobass:   7b a3 fe 87 ff 91 7f 8c  (top-left)
notreb:    fd de 79 17 ff 91 7f 8c  (bottom-left)
allbass:   fd f0 79 5d 7f 8c ff 91   (bottom-right)
alltreb:    78 30 fd a2 7f 8c ff 91   (top-right)

are these coordinates on the circle? or bars?
B = emph. bass; M = emph. mids; T : emph. treb

     B-  MT  T+
  M       =      BT
    T-   MB  B+

ANC read (03-01-07/01 transp, 03-00-02/00 norm, 03-00-01 anc): 

gatttool -b {MACADDR} --char-read --uuid=4446cf5f-12f2-4c1e-afe1-b15797535ba8 | sed "s/.*value: 03 0[12] //" | sed "s/01/ANC/" |  sed "s/00/No_ANC/"


from Mi to HP, ATT, len 13, write request, handle 0x003c

02 03 00 08 00 04 00 04 00 12 3c 00 04

gatttool -b {MACADDR} --char-write-req --handle=0x003c --value="02 03 00 08 00 04 00 04 00 12 3c 00 04" --listen


write response:
handle 0x003c
